FROM python:2.7
MAINTAINER Edgar Ruben
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
CMD ["python", "app.py"]
